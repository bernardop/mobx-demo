const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const LodashModuleReplacementPlugin = require("lodash-webpack-plugin");
const webpack = require("webpack");
const merge = require("webpack-merge");

const TARGET = process.env.npm_lifecycle_event;
const ROOT_PATH = path.join(__dirname);
const SRC_PATH = path.join(ROOT_PATH, "src");
const ASSETS_PATH = path.join(ROOT_PATH, "assets");

process.env.BABEL_ENV = TARGET;

const common = {
    entry: SRC_PATH,
    output: {
        publicPath: "/",
        filename: "bundle.js",
        path: path.join(ROOT_PATH, "build"),
    },
    resolve: {
        extensions: ["", ".js", ".jsx"],
    },
    module: {
        loaders: [
            {
                test: /\.css$/,
                loaders: ["style", "css"],
                include: [
                    SRC_PATH,
                ],
            },
            {
                test: /\.jsx?$/,
                loaders: ["babel"],
                include: SRC_PATH,
            },
            {
                test: /\.json$/,
                loaders: ["json"],
                include: SRC_PATH,
            },
            {
                test: /\.(jpg|png)$/,
                loader: "url?limit=25000",
                include: ASSETS_PATH,
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: "MobX Demo",
            template: "src/index.html",
            inject: "body",
        }),
        new LodashModuleReplacementPlugin(),
    ],
};

if (TARGET === "start" || !TARGET) {
    module.exports = merge(common, {
        devtool: "eval-source-map",
        devServer: {
            historyApiFallback: true,
            hot: true,
            inline: true,
            progress: true,
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
        ],
    });
}
