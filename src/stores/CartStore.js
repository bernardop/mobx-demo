import { observable, computed, action } from "mobx";

import { Pizza } from "./PizzaStore";

class CartStore {
    @observable pizzas = [];

    @action addPizza = function (pizza) {
        this.pizzas.push(new Pizza(pizza.store, pizza));
    }

    @action checkout = function () {
        this.pizzas = [];
    }

    @computed get cartTotal() {
        return this.pizzas.reduce((prevTotal, currentPizza) => prevTotal + currentPizza.totalCost, 0);
    }
}

const cartStore = new CartStore();

export default cartStore;
export { CartStore };
