import { observable, action } from "mobx";

import getData from "../api/api";

class Topping {
    id;
    @observable code;
    @observable cost;
    @observable name;

    constructor({ id, code, cost, name }) {
        this.id = id;
        this.code = code;
        this.cost = cost;
        this.name = name;
    }
}

class ToppingStore {
    @observable toppings = [];

    constructor() {
        this.loadToppings();
    }

    @action loadToppings = function () {
        getData(action((data) => {
            this.toppings = data.toppings.map((topping) => new Topping(topping));
        }));
    }
}

const toppingStore = new ToppingStore();

export default toppingStore;
export { Topping, ToppingStore };
