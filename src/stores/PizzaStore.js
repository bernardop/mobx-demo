import { observable, computed, action } from "mobx";

import getData from "../api/api";

class Pizza {
    id;
    store;
    @observable size;
    @observable name;
    @observable image;
    @observable toppings = [];

    constructor(store, { id, size, name, image, toppings }) {
        this.id = id;
        this.store = store;
        this.size = size;
        this.name = name;
        this.image = image;
        this.toppings = toppings;
    }

    @computed get toppingsString() {
        const context = this;
        return context.store.availableToppings
            .filter((topping) => context.toppings.indexOf(topping.code) >= 0)
            .map((topping) => topping.name).join(", ");
    }

    @computed get baseCost() {
        switch (this.size) {
        case "small":
            return 6.50;
        case "medium":
            return 7.50;
        case "large":
            return 8.50;
        default:
            return 6.50;
        }
    }

    @computed get totalCost() {
        const context = this;
        return context.store.availableToppings
            .filter((topping) => context.toppings.indexOf(topping.code) >= 0)
            .reduce((prevTotal, currentTopping) => prevTotal + currentTopping.cost, context.baseCost);
    }
}

class PizzaStore {
    @observable pizzas = [];
    @observable availableToppings = [];
    @observable pizzasLoaded = false;

    constructor() {
        this.loadPizzas();
    }

    @action loadPizzas = function () {
        const context = this;
        getData(action((data) => {
            context.pizzas = data.pizzas.map((pizza) => new Pizza(context, pizza));
            context.availableToppings = data.toppings;
            context.pizzasLoaded = true;
        }));
    }

    @action setPizzaSize = function (pizzaId, size) {
        const pizzaRef = this.pizzas.find((pizza) => pizza.id === pizzaId);
        pizzaRef.size = size;
    }

    @action toggleTopping = function (pizzaId, toppingCode) {
        const pizzaRef = this.pizzas.find((pizza) => pizza.id === pizzaId);
        if (pizzaRef.toppings.findIndex((topping) => topping === toppingCode) >= 0) {
            pizzaRef.toppings.splice(pizzaRef.toppings.findIndex((topping) => topping === toppingCode), 1);
        } else {
            pizzaRef.toppings.push(toppingCode);
        }
    }
}

const pizzaStore = new PizzaStore();
export default pizzaStore;
export { Pizza, PizzaStore };
