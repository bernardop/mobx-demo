import React from "react";
import { render } from "react-dom";
import { useStrict } from "mobx";
import { Provider } from "mobx-react";

import App from "./components/App";
import pizzaStore from "./stores/PizzaStore";
import toppingStore from "./stores/ToppingStore";
import cartStore from "./stores/CartStore";

useStrict(true);

const store = {
    pizzaStore,
    toppingStore,
    cartStore,
};

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById("app")
);
