import pizzas from "./pizzas.json";

const TIMEOUT = 500;

const getData = (cb, timeout) => {
    setTimeout(() => {
        cb(pizzas);
    }, timeout || TIMEOUT);
};

export default getData;
