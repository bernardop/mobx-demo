import React, { Component, PropTypes } from "react";
import { observer, inject } from "mobx-react";

import { Pizza } from "../stores/PizzaStore";
import ToppingItem from "./ToppingItem";

@inject("store")
@observer
class ToppingsContainer extends Component {
    constructor(props) {
        super(props);

        this.toggleTopping = this.toggleTopping.bind(this);
    }

    toggleTopping(pizzaId, toppingCode) {
        this.props.store.pizzaStore.toggleTopping(pizzaId, toppingCode);
    }

    render() {
        const { pizza } = this.props;
        const { toppings } = this.props.store.toppingStore;
        return (
            <fieldset>
                <legend>Choose Toppings</legend>
                {toppings.map((topping) => (
                    <ToppingItem key={`${pizza.id}-${topping.code}`} topping={topping} pizzaId={pizza.id} selectedToppings={pizza.toppings}
                        toggleToppingHandler={this.toggleTopping}
                    />
                ))}
            </fieldset>
        );
    }
}

ToppingsContainer.wrappedComponent.propTypes = {
    store: PropTypes.object,
    pizza: PropTypes.instanceOf(Pizza),
};

export default ToppingsContainer;
