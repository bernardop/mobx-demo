import React, { PropTypes } from "react";
import { observer } from "mobx-react";

import { Pizza } from "../stores/PizzaStore";
import ToppingsContainer from "./ToppingsContainer";
import Sizes from "./Sizes";

const PizzaItem = observer(({ pizza, sizeChangeHandler, orderPizzaHandler }) => (
    <div className="columns small-12 medium-4" key={pizza.id}>
        <div className="callout">
            <h5>{pizza.name}</h5>
            <p className="text-center">
                <img className="thumbnail" alt={pizza.name} src={`../../assets/${pizza.image}`} />
            </p>
            <button type="button" className="expanded button" onClick={() => orderPizzaHandler(pizza)}>Order</button>
            <Sizes currentSize={pizza.size} pizzaId={pizza.id} sizeChangeHandler={sizeChangeHandler} />
            <ToppingsContainer pizza={pizza} />
            <p className="pizza-total">Total: ${pizza.totalCost.toFixed(2)}</p>
        </div>
    </div>
));

PizzaItem.propTypes = {
    pizza: PropTypes.instanceOf(Pizza),
    sizeChangeHandler: PropTypes.func,
    orderPizzaHandler: PropTypes.func,
};

export default PizzaItem;
