import React, { PropTypes } from "react";
import { observer } from "mobx-react";

const Sizes = observer(({ currentSize, pizzaId, sizeChangeHandler }) => (
    <fieldset>
        <legend>Choose Your Pizza Size</legend>
        <input type="radio" name={`${pizzaId}-small`} value="small" id={`${pizzaId}-small`} checked={currentSize === "small"}
            onChange={() => sizeChangeHandler(pizzaId, "small")}
        />
        <label htmlFor={`${pizzaId}-small`}>Small</label>

        <input type="radio" name={`${pizzaId}-medium`} value="medium" id={`${pizzaId}-medium`} checked={currentSize === "medium"}
            onChange={() => sizeChangeHandler(pizzaId, "medium")}
        />
        <label htmlFor={`${pizzaId}-medium`}>Medium</label>

        <input type="radio" name={`${pizzaId}-large`} value="large" id={`${pizzaId}-large`} checked={currentSize === "large"}
            onChange={() => sizeChangeHandler(pizzaId, "large")}
        />
        <label htmlFor={`${pizzaId}-large`}>Large</label>
    </fieldset>
));

Sizes.propTypes = {
    currentSize: PropTypes.string,
    pizzaId: PropTypes.number,
    sizeChangeHandler: PropTypes.func,
};

export default Sizes;
