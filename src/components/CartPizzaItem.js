import React, { PropTypes } from "react";

import { Pizza } from "../stores/PizzaStore";

const CartPizzaItem = ({ pizza }) => (
    <tr>
        <td>
            <span className="cart-pizza-name">{pizza.name}</span> - <span className="cart-pizza-size">{pizza.size}</span><br />
            <span className="cart-pizza-toppings">{pizza.toppingsString}</span>
        </td>
        <td>${pizza.totalCost.toFixed(2)}</td>
    </tr>
);

CartPizzaItem.propTypes = {
    pizza: PropTypes.instanceOf(Pizza),
};

export default CartPizzaItem;
