import React, { Component, PropTypes } from "react";
import { observer, inject } from "mobx-react";

import PizzaItem from "./PizzaItem";

@inject("store")
@observer
class PizzasContainer extends Component {
    constructor(props) {
        super(props);

        this.renderPizza = this.renderPizza.bind(this);
        this.handleSizeChange = this.handleSizeChange.bind(this);
        this.handleOrderPizza = this.handleOrderPizza.bind(this);
    }

    handleSizeChange(pizzaId, size) {
        this.props.store.pizzaStore.setPizzaSize(pizzaId, size);
    }

    handleOrderPizza(pizza) {
        this.props.store.cartStore.addPizza(pizza);
    }

    renderPizza() {
        const pizzaStore = this.props.store.pizzaStore;
        if (pizzaStore.pizzasLoaded) {
            return (
                <div className="row">
                    {pizzaStore.pizzas.map((pizza) => (
                        <PizzaItem key={pizza.id} pizza={pizza} sizeChangeHandler={this.handleSizeChange}
                            orderPizzaHandler={this.handleOrderPizza}
                        />
                    ))}
                </div>
            );
        }
        return (<div className="row">Loading...</div>);
    }

    render() {
        return (
            <div className="columns small-9">
                {this.renderPizza()}
            </div>
        );
    }
}

PizzasContainer.wrappedComponent.propTypes = {
    store: PropTypes.object,
};

export default PizzasContainer;
