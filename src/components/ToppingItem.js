import React, { PropTypes } from "react";
import { observer, propTypes } from "mobx-react";

import { Topping } from "../stores/ToppingStore";

const ToppingItem = observer(({ topping, pizzaId, selectedToppings, toggleToppingHandler }) => {
    const toppingIdentifier = `${pizzaId}-${topping.code}`;
    return (
        <div>
            <input id={toppingIdentifier} name={toppingIdentifier} type="checkbox"
                checked={selectedToppings.filter((selectedTopping) => selectedTopping === topping.code).length > 0}
                onChange={() => toggleToppingHandler(pizzaId, topping.code)}
            />
            <label key={topping.id} htmlFor={toppingIdentifier}>{topping.name} - ${topping.cost.toFixed(2)}</label>
        </div>
    );
});

ToppingItem.propTypes = {
    topping: PropTypes.instanceOf(Topping),
    pizzaId: PropTypes.number,
    selectedToppings: propTypes.observableArray,
    toggleToppingHandler: PropTypes.func,
};

export default ToppingItem;
