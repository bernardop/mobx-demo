import React, { PropTypes } from "react";

const Header = ({ title }) => (
    <div className="row align-center">
        <h1>{title}</h1>
    </div>
);

Header.propTypes = {
    title: PropTypes.string,
};

export default Header;
