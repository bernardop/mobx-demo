import React, { Component, PropTypes } from "react";
import { observable, action } from "mobx";
import { observer, inject } from "mobx-react";

import CartPizzaItem from "./CartPizzaItem";

@inject("store")
@observer
class CartContainer extends Component {
    constructor(props) {
        super(props);

        this.renderCart = this.renderCart.bind(this);
        this.handleCheckout = this.handleCheckout.bind(this);
        this.renderOrderSuccess = this.renderOrderSuccess.bind(this);
    }

    @observable orderPlaced = false;

    @action handleCheckout() {
        this.props.store.cartStore.checkout();
        this.orderPlaced = true;
        setTimeout(action(() => (this.orderPlaced = false)), 2000);
    }

    renderOrderSuccess() {
        if (this.orderPlaced) {
            return (
                <div className="callout success">Thank You for your order</div>
            );
        }
        return null;
    }

    renderCart() {
        const cartStore = this.props.store.cartStore;
        if (cartStore.pizzas.length > 0) {
            return (
                <table>
                    <tbody>
                        {cartStore.pizzas.map((pizza) => (
                            <CartPizzaItem key={pizza.id} pizza={pizza} />
                        ))}
                    </tbody>
                </table>
            );
        }
        return (<div className="cart-empty">Cart is empty</div>);
    }

    render() {
        return (
            <div className="columns">
                <div className="callout primary">
                    <h5>Cart</h5>
                    <button className="button alert expanded"
                        disabled={this.props.store.cartStore.pizzas.length === 0}
                        onClick={this.handleCheckout}
                    >Checkout</button>
                    {this.renderCart()}
                    <p className="cart-total">Total: ${this.props.store.cartStore.cartTotal.toFixed(2)}</p>
                </div>
                {this.renderOrderSuccess()}
            </div>
        );
    }
}

CartContainer.wrappedComponent.propTypes = {
    store: PropTypes.object,
};

export default CartContainer;
