import React from "react";
import DevTools from "mobx-react-devtools";

import Header from "./Header";
import PizzasContainer from "./PizzasContainer";
import CartContainer from "./CartContainer";
import "../styles/app.css";

const App = () => (
    <div>
        <Header title="Bernando's" />
        <div className="row">
            <PizzasContainer />
            <CartContainer />
        </div>
        <DevTools />
    </div>
);

export default App;
